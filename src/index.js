/* global React ReactDOM */
const App = () => {
  return React.createElement(
    "h1",
    {
      id: "myHeader",
      className: "my-header",
    },
    "Hello World"
  );
};

ReactDOM.render(React.createElement(App), document.getElementById("root"));
