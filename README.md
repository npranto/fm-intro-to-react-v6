# fm-intro-to-react-v6

### Steps

- Sets up index.html, index.css and index.js files inside "src" directory
- Link index.css and index.js files inside index.html file
- Add React and ReactDOM CDN script tags to start using React through window object (Ref: https://reactjs.org/docs/cdn-links.html)
- Create React elements with React.createElement function
- Use ReactDOM.render function to render React element to the DOM
- Setup npm inside your project to install 3rd party packages from NPM registry
- Add prettier (`npm i -D prettier`) package; add `.prettierrc` file in root; add "format" script to format source code; install prettier VSCode extension and setup auto format on save and set require prettier config file as well
- Add eslint packages along with eslint-config-prettier packages to setup linting (`npm install -D eslint@7.18.0 eslint-config-prettier@8.1.0`); install eslint VSCode extension; create `.eslintrc.json` file in root, setup default configuration

```json
{
  "extends": ["eslint:recommended", "prettier"],
  "plugins": [],
  "parserOptions": {
    "ecmaVersion": 2021,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "env": {
    "browser": true,
    "es6": true,
    "node": true
  }
}
```

- Setup git to track your source code
- Adds .gitignore file to root (Ref: https://raw.githubusercontent.com/github/gitignore/master/Node.gitignore)
